import { eleves, eleves_groupes, groupes } from "@prisma/client";
import { boolean } from "joi";
import { prisma } from "./prisma";

export type groupe_with_eleves = (groupes & {
    ELEVES: (eleves_groupes & {
        eleve: eleves;
    })[];
})


export async function createGroup(id : string, list_eleves : eleves[]): Promise<groupe_with_eleves> { 
    return await prisma.groupes.create({
        data:{
            id,
            ELEVES : {
                create : list_eleves.map(eleve => ({
                    eleve_id : eleve.id                      
                }))
            }           
        },
        include:{
            ELEVES:{
                include:{
                    eleve: true
                }
            }
        }
    })
}


export async function getGroup(id : string): Promise<groupe_with_eleves | null> {
    return await prisma.groupes.findUnique({
        where:{
            id
        },
        include:{
            ELEVES:{
                include:{
                    eleve: true
                }
            }
        }

    })

}


export function groupeHasEleve(groupe : groupe_with_eleves, eleve : eleves) : boolean {
    const list_eleves = getGroupeEleves(groupe)
    return Boolean(list_eleves.find(eleve_find => eleve_find.id  ==  eleve.id))
}


export async function addEleve(groupe : groupe_with_eleves, eleve : eleves) : Promise<groupe_with_eleves> {
    return await prisma.groupes.update({
        where:{
            id : groupe.id
        },
        data:{
            ELEVES : {
                create : {
                    eleve_id : eleve.id
                }
            }
        },
        include:{
            ELEVES:{
                include:{
                    eleve: true
                }
            }
        }
    })
}


export async function deleteEleve(groupe : groupe_with_eleves, eleve : eleves) : Promise<groupe_with_eleves> {
    return await prisma.groupes.update({
        where:{
            id : groupe.id
        },
        data:{
            ELEVES : {
                deleteMany : {
                    eleve_id : eleve.id
                }
            }
        },
        include:{
            ELEVES:{
                include:{
                    eleve: true
                }
            }
        }
    })
}



export function getGroupeEleves(groupe : groupe_with_eleves) : eleves[] {
    return groupe.ELEVES.map(eleves_groupes => eleves_groupes.eleve)
}

export async function deleteGroupe(groupe : groupes){
   await prisma.groupes.delete({
        where:{
            id : groupe.id 
        }
    })
}

export function groupeHasRoom(groupe : groupe_with_eleves) {
    return groupe.ELEVES.length < 10

}

export function groupeRoom(groupe : groupe_with_eleves) {
    return groupe.ELEVES.length

}


export function validateOption(eleves1 : eleves[], eleves2 : eleves[]) : boolean {
    const list_eleves = [...eleves1, ...eleves2]
    const nb_latin = list_eleves.reduce((acc, eleve) => acc + (eleve.LATIN ? 1 : 0), 0)
    const nb_maths = list_eleves.reduce((acc, eleve) => acc + (eleve.MATHS ? 1 : 0), 0)
    const nb_eco = list_eleves.reduce((acc, eleve) => acc + (eleve.ECO ? 1 : 0), 0)

    return (nb_latin == 0 || nb_maths == 0 || nb_eco == 0)
}


