import { eleves } from "@prisma/client";
import { prisma } from "./prisma";


export async function getElevesFromIds(ids_eleves : number[]): Promise<eleves[]> {
    return await prisma.eleves.findMany({
        where:{
            id : {
                in : ids_eleves
            }
        }
    })

}



export async function getEleveFromId(id : number): Promise<eleves|null> {
    return await prisma.eleves.findUnique({
        where: {
            id
        }
    })

}