path = require('path');
require('dotenv').config({ path: path.resolve('./.env') });
const mysql = require('mysql2');
module.exports = mysql.createConnection({
    host: process.env.DB_HOST_LOG,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
});