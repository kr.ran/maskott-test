import { SEXE } from '@prisma/client';
import express from 'express'
import { getEleveFromId, getElevesFromIds } from './eleve';
import { addEleve, createGroup, deleteEleve, deleteGroupe, getGroup, getGroupeEleves, groupeHasEleve, groupeRoom, validateOption } from './groups';
import { prisma } from './prisma'
const app = express();
app.use(express.json());



app.get('/api/eleves', async (req, res) => {
    
    try{
        const latin = req.query.latin ? req.query.latin == 'true' : undefined
        const maths = req.query.maths ? req.query.maths == 'true' : undefined
        const eco = req.query.eco ? req.query.eco == 'true' : undefined
        const sexe = req.query.sexe as SEXE | undefined;
        const born_after = req.query.born_after ? new Date(req.query.born_after as string) : undefined
        const born_before =  req.query.born_before ? new Date(req.query.born_before as string) : undefined

       
        const data = await prisma.eleves.findMany({
            where: {
                DATE_NAISSANCE: {
                    gte: born_after,
                    lte: born_before
                },
                SEXE: sexe,
                LATIN: latin,
                MATHS: maths,
                ECO: eco
            }
        })
        
        res.json(data)

    }
    catch (err){
        console.log(err)
        res.send(500)
    } 
});



app.post('/api/groupes', async (req, res) => {
    try{
        const id_eleves = req.body.eleves;
        const nom_groupe = req.body.nom;
        if(!nom_groupe || await getGroup(nom_groupe)) throw "Le nom du groupe ne peut pas etre vide ou déjà existant"
        if(!id_eleves || id_eleves.length < 3) throw "liste des élèves ne peut pas etre vide ou inférieur à 3"

        const lst_eleves = await getElevesFromIds(id_eleves)
        if(!validateOption(lst_eleves, [])) throw "il ne peut pas y avoir plus de deux options"
        res.json(await createGroup(nom_groupe, lst_eleves))

    }
    catch (err){
        console.log(err)
        res.status(500).json({ err })
    } 
})



app.post('/api/groupes/modifier', async (req, res) => {
    try{
        const id_eleve = req.body.eleve;
        const nom_groupe = req.body.nom;
        const action = req.body.action;

    

        if(!nom_groupe) throw "nom groupe ne peut pas etre vide"
        if(!id_eleve) throw "l'id de l'eleve ne peut pas etre vide"

        const groupe = await getGroup(nom_groupe)
        
        if(!groupe) throw "veuillez choisir un nom de groupe existant"
        
        const eleve = await getEleveFromId(id_eleve)

        if(!eleve) throw "veuillez choisir un eleve existant"


        switch (action) {
            case "ajouter":
                if (groupeRoom(groupe) == 10)
                    throw "plus de place dans le groupe"
                if (groupeHasEleve(groupe, eleve))
                    throw "l'eleve est déja dans le groupe"
                if (!validateOption(getGroupeEleves(groupe), [eleve]))       
                    throw "mauvaise option"   
                res.json(await addEleve(groupe, eleve))
                break;
            case "supprimer":
                if (!groupeHasEleve(groupe, eleve))
                    throw "l'eleve n'est pas dans le groupe"
                if (groupeRoom(groupe) == 3)
                    throw "on ne peut pas supprimer plus d'eleves"
                res.json(await deleteEleve(groupe, eleve))
                break;
            default:
                throw "veuillez definir l'action";
        }

    }
    catch (err){
        console.log(err)
        res.status(500).json({ err })
    } 
})


app.post('/api/groupes/supprimer', async (req, res) => {

    try{

        const nom_groupe = req.body.nom;
        const groupe = await getGroup(nom_groupe)
        if(!groupe) throw "veuillez choisir un nom de groupe existant"
        await deleteGroupe(groupe)
        res.json("le groupe à été supprimé")

        
    }
    catch (err){
        console.log(err)
        res.status(500).json({ err })
    }

})



const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}`));
