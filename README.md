# Maskott Test



## Installation de l'environnement technique

Etapes à suivre : 


 - Création d'une base de données vide.
 - Création d'un .env à partir de l'exemple .env_example
 - Importation de la base de données : 
```
npx prisma migrate dev
```
- Lancement de prisma studio pour visualiser les tables et pouvoir les modifier :
```
npx prisma studio
```

- Il ne reste plus qu'à lancer et à tester :
```
npm run start
```

- Le port 3000 est utilisé par défaut, pour changer cela il faut définir une variable d'environnement "PORT".

## Questions / Réponses

- Avant de commencer à développer, j'avais estimé le temps pour faire ce projet entre 2 et 4h.

- J'ai crée deux versions du projet, une par soirée. La première version a été réalisée avec Node.js et sans orm.
  Voulant aller plus loin, j'ai décidé d'implémenter une autre version en utilisant prisma et TypeScript.

- Les difficultés que j'ai rencontrées venaient du fait que je n'avais jamais utilisé prisma, et donc j'ai du régler pas mal 
  d'erreurs de ma part suite à ma méconnaissance de cette technologie

- Pour sécuriser mon API, je mettrai en place : un système de token, le protocole oauth2, un chiffrage des données, et un quota de requêtes par jour.
