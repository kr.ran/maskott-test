/*
  Warnings:

  - A unique constraint covering the columns `[NOM]` on the table `groupes` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX `groupes_NOM_key` ON `groupes`(`NOM`);
