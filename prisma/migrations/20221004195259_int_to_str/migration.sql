/*
  Warnings:

  - The primary key for the `eleves_groupes` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `groupes` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `NOM` on the `groupes` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE `eleves_groupes` DROP FOREIGN KEY `eleves_groupes_groupe_id_fkey`;

-- DropIndex
DROP INDEX `groupes_NOM_key` ON `groupes`;

-- AlterTable
ALTER TABLE `eleves_groupes` DROP PRIMARY KEY,
    MODIFY `groupe_id` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`eleve_id`, `groupe_id`);

-- AlterTable
ALTER TABLE `groupes` DROP PRIMARY KEY,
    DROP COLUMN `NOM`,
    MODIFY `id` VARCHAR(191) NOT NULL,
    ADD PRIMARY KEY (`id`);

-- AddForeignKey
ALTER TABLE `eleves_groupes` ADD CONSTRAINT `eleves_groupes_groupe_id_fkey` FOREIGN KEY (`groupe_id`) REFERENCES `groupes`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
