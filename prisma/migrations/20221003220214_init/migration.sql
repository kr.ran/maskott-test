-- CreateTable
CREATE TABLE `eleves` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `PRENOM` VARCHAR(191) NOT NULL,
    `NOM` VARCHAR(191) NOT NULL,
    `EMAIL` VARCHAR(191) NOT NULL,
    `DATE_NAISSANCE` DATETIME(3) NOT NULL,
    `SEXE` VARCHAR(191) NOT NULL,
    `LATIN` BOOLEAN NOT NULL,
    `MATHS` BOOLEAN NOT NULL,
    `ECO` BOOLEAN NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `groupes` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `NOM` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `eleves_groupes` (
    `eleve_id` INTEGER NOT NULL,
    `groupe_id` INTEGER NOT NULL,

    PRIMARY KEY (`eleve_id`, `groupe_id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `eleves_groupes` ADD CONSTRAINT `eleves_groupes_eleve_id_fkey` FOREIGN KEY (`eleve_id`) REFERENCES `eleves`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `eleves_groupes` ADD CONSTRAINT `eleves_groupes_groupe_id_fkey` FOREIGN KEY (`groupe_id`) REFERENCES `groupes`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
