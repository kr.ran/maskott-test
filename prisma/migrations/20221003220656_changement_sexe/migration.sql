/*
  Warnings:

  - You are about to alter the column `SEXE` on the `eleves` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Enum("eleves_SEXE")`.

*/
-- AlterTable
ALTER TABLE `eleves` MODIFY `SEXE` ENUM('H', 'F') NOT NULL;
